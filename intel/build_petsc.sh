#!/bin/bash
source env_build.sh

# VERSION="3.8.3"
VERSION="3.11.3"

# export CXXFLAGS="${CXXFLAGS} -std=c++0x"

echo "Downloading and building PETSc ${VERSION}"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --quiet --read-timeout=10 -nc -P tar/ http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${VERSION}.tar.gz && \
   tar -xzf tar/petsc-lite-${VERSION}.tar.gz && \
   cd petsc-${VERSION} && \
     python2 ./configure \
        --with-cxx=$MPICXX \
        --with-cc=$MPICC \
        --with-fc=$MPIF90 \
        --with-mpiexec=$MPIEXEC \
        --COPTFLAGS="-O3 -xCORE-AVX2" \
        --CXXOPTFLAGS="-O3 -xCORE-AVX2" \
        --FOPTFLAGS="-O3 -xCORE-AVX2" \
        --with-c-support \
        --with-shared-libraries \
        --with-debugging=0 \
        --with-blaslapack-dir=${MKLROOT} \
        --download-hypre \
        --download-mumps \
        --download-metis \
        --download-parmetis \
        --download-suitesparse \
        --download-ptscotch \
        --download-scalapack \
        --download-blacs \
        --prefix=${PREFIX} && \
    make MAKE_NP=${BUILD_THREADS} && make install
        # --known-mpi-shared-libraries=1 \
        # --with-cc=$MPICC \
        # --with-cxx=$MPICXX \
        # --with-fc=$MPIF90 \
        # --download-ml \
        # --download-superlu_dist \
        # --with-cxx-dialect=C++11 CXXFLAGS="-std=c++0x" \

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

