#!/bin/bash
source env_build.sh

pip install -I pew --prefix=$PREFIX
pew new -d  fenics-${TAG} -i ply 
# pew in fenics-${TAG} pew add ${EBROOTPYTHON}/lib/python3.6/site-packages
# include system mpi4py
pew in fenics-${TAG} pew toggleglobalsitepackages


if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi
