#!/bin/bash
source env_build.sh

VERSION="3.8.3"

echo "Downloading and building PETSc ${VERSION}"

mkdir -p $BUILD_DIR/tar

cd ${BUILD_DIR} && \
   wget --quiet --read-timeout=10 -nc -P tar/ http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${VERSION}.tar.gz && \
   tar -xzf tar/petsc-lite-${VERSION}.tar.gz && \
   cd petsc-${VERSION} && \
   python2 ./configure \
        --with-cxx=$MPICXX \
        --with-cc=$MPICC \
        --with-fc=$MPIF90 \
        --with-mpiexec=$MPIEXEC \
        --COPTFLAGS="-O3 -mtune=native -ffast-math" \
        --CXXOPTFLAGS="-O3 -mtune=native -ffast-math" \
        --FOPTFLAGS="-O3 -mtune=native -ffast-math" \
        --with-c-support \
        --with-shared-libraries \
        --with-blaslapack-dir=${EBROOTOPENBLAS} \
        --with-debugging=0 \
        --download-hypre \
        --download-mumps \
        --download-ml \
        --download-metis \
        --download-parmetis \
        --download-suitesparse \
        --download-ptscotch \
        --download-scalapack \
        --download-blacs \
        --download-superlu_dist \
        --prefix=${PREFIX} && \
    make MAKE_NP=${BUILD_THREADS} && make install
        # --with-blaslapack-dir=${MKLROOT} \

if [ "$continue_on_key" = true ]; then
    echo "Press any key to continue..."
    read -n 1
fi

