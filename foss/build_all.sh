#!/bin/bash

# exit on error
set -e

# FENICS version
export FENICS_VERSION="2017.2.0.post0"
# export FENICS_VERSION="master"

# TAG that specifies the name of the build directories and the virtualenv
export TAG="${FENICS_VERSION}-foss2018a"

export BUILD_THREADS=4
export PREFIX=${HOME}/dev/fenics-${TAG}
export BUILD_DIR=${HOME}/dev/build/fenics-${TAG}
mkdir -p ${PREFIX}

export PYTHON_VERSION="3.6"
# set this to true in order to wait after each module
export continue_on_key=false

echo "Installing FEniCS to ${PREFIX}"

./setup_virtualenv.sh  # if not, make sure virtualenv fenics-${tag} exists!!
# ./build_cmake.sh
# ./build_eigen.sh    # worked with lmod modules before!!!
./build_petsc.sh
./build_python_modules.sh
./build_fenics_pymodules.sh  # ffc fiat ufl uflacs instant
# ./build_doxygen.sh  # system version 1.8.8 not working with dolfin, 1.8.13 reported buggy, use 1.8.12   (sep2017)
./build_dolfin.sh
./setup_fenics_env.sh

# run with $ ./build_all.sh |& tee -a build.log
